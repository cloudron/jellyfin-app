On first visit, you can setup an admin user and finish the installation.

**IMPORTANT:** Do not uncheck `Allow remote connections to this server` when
completing the installation. Doing so will make the server unreachable.

