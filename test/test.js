#!/usr/bin/env node

/* jshint esversion: 8 */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const ADMIN_USERNAME='admin';
    const ADMIN_PASSWORD='changeme';
    const BBB_LOCATION = path.join(__dirname, 'bbb.mp4');
    const LOCATION = process.env.LOCATION || 'test';
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 30000;

    let browser, app;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function clearCache() {
        await browser.manage().deleteAllCookies();
        await browser.quit();
        browser = null;
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        chromeOptions.addArguments(`--user-data-dir=${await fs.promises.mkdtemp('/tmp/test-')}`); // --profile-directory=Default
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    async function setupWizard() {
        await browser.manage().deleteAllCookies();
        await browser.get('https://' + app.fqdn);
        await waitForElement(By.id('selectLocalizationLanguage'));
        await browser.findElement(By.xpath('//button[@type="submit"]')).click();
        await waitForElement(By.id('txtUsername'));
        await browser.sleep(2000);
        await browser.findElement(By.id('txtUsername')).clear();
        await browser.findElement(By.id('txtUsername')).sendKeys(ADMIN_USERNAME);
        await browser.findElement(By.xpath('//div[@id="wizardUserPage" ]//input[@id="txtManualPassword"]')).clear();
        await browser.findElement(By.xpath('//div[@id="wizardUserPage" ]//input[@id="txtManualPassword"]')).sendKeys(ADMIN_PASSWORD);
        await browser.findElement(By.id('txtPasswordConfirm')).clear();
        await browser.findElement(By.id('txtPasswordConfirm')).sendKeys(ADMIN_PASSWORD);
        await browser.findElement(By.xpath('//div[@id="wizardUserPage" ]//button[.//span[contains(text(), "Next")]]')).click();
        await waitForElement(By.xpath('//div[contains(@class, "editLibrary")]'));
        await browser.findElement(By.xpath('//div[contains(@class, "editLibrary")]')).click();
        await waitForElement(By.id('selectCollectionType'));
        await browser.executeScript('document.getElementById("selectCollectionType").selectedIndex = 1');
        await browser.findElement(By.id('txtValue')).sendKeys('Movies');
        await browser.findElement(By.xpath('//button[contains(@class, "btnAddFolder")]')).click();
        await waitForElement(By.id('txtDirectoryPickerPath'));
        await browser.findElement(By.id('txtDirectoryPickerPath')).sendKeys('/app/data/files/Movies');
        await browser.findElement(By.xpath('//button[contains(@class, "btnRefreshDirectories")]')).click();
        await browser.findElement(By.xpath('//form[.//input[@id="txtDirectoryPickerPath"]]')).submit();
        await waitForElement(By.xpath('//button[.//span[contains(text(), "Ok")]]'));
        await browser.findElement(By.xpath('//button[.//span[contains(text(), "Ok")]]')).click();
        await waitForElement(By.xpath('//div[@id="wizardLibraryPage"]//button[.//span[contains(text(), "Next")]]'));
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//div[@id="wizardLibraryPage"]//button[.//span[contains(text(), "Next")]]')).click();
        await waitForElement(By.xpath('//div[.//h1[contains(text(), "Preferred Metadata Language")] and @id="wizardSettingsPage" ]//button[.//span[contains(text(), "Next")]]'));
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//div[.//h1[contains(text(), "Preferred Metadata Language")] and @id="wizardSettingsPage" ]//button[.//span[contains(text(), "Next")]]')).click();
        await waitForElement(By.xpath('//div[.//h1[contains(text(), "Remote Access")] and @id="wizardSettingsPage" ]//button[.//span[contains(text(), "Next")]]'));
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//div[.//h1[contains(text(), "Remote Access")] and @id="wizardSettingsPage" ]//button[.//span[contains(text(), "Next")]]')).click();
        await waitForElement(By.xpath('//button[.//span[contains(text(), "Finish")]]'));
        await browser.findElement(By.xpath('//button[.//span[contains(text(), "Finish")]]')).click();
        await waitForElement(By.id('txtManualName'));
    }

    async function login(username, password) {
        for (let i = 0; i < 5; i++) {
            // https://github.com/jellyfin/jellyfin/issues/11597 https://github.com/jellyfin/jellyfin/issues/5575 etc
            await browser.get(`https://${app.fqdn}`);
            await browser.sleep(3000);
            const currentUrl = await browser.getCurrentUrl();
            if (currentUrl.includes('login.html')) break;
            console.log(`attempt ${i} . Some caching issue, it is not redirecting to login page and instead to select server page`);
            await clearCache();
        }

        await waitForElement(By.id('txtManualName'));
        await browser.findElement(By.id('txtManualName')).sendKeys(username);
        await browser.findElement(By.id('txtManualPassword')).sendKeys(password);
        await browser.findElement(By.xpath('//button[@type="submit"]')).click();
        await waitForElement(By.xpath('//div[text() = "Home"]'));
    }

    async function logout() {
        await browser.get('https://' + app.fqdn + '/web/index.html#!/mypreferencesmenu.html');
        await waitForElement(By.className('mainDrawerButton'));
        await browser.sleep(2000);
        await browser.findElement(By.className('mainDrawerButton')).click();
        await waitForElement(By.className('btnLogout'));
        await browser.sleep(2000);
        await browser.findElement(By.className('btnLogout')).click();
        await browser.wait(until.elementLocated(By.id('txtManualName')), TEST_TIMEOUT);
    }

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION || a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    }

    async function contentAdd() {
        execSync(`cloudron push ${BBB_LOCATION} /app/data/files/Movies/BBB.mp4 --app ${LOCATION}`, EXEC_ARGS);

        await browser.get(`https://${app.fqdn}/web/#/dashboard`);
        await waitForElement(By.xpath('//button[contains(., "Scan All Libraries")]'));
        await browser.findElement(By.xpath('//button[contains(., "Scan All Libraries")]')).click();

        // jellyfin takes it's sweet little time to scan...
        console.log('Waiting for 10 seconds for scan');
        await browser.sleep(10000);
    }

    async function contentExists() {
        await browser.get('https://' + app.fqdn + '/web/index.html');
        await waitForElement(By.xpath('//a[.="BBB"]'));
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', async function () {
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        await browser.sleep(20000);
    });

    it('can get app information', getAppInfo);

    it('setup wizard', setupWizard);
    it('clear cache', clearCache);
    it('can login', login.bind(null, process.env.USERNAME, process.env.PASSWORD));
    it('can logout', logout);

    it('clear cache', clearCache);
    it('can login as admin', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));
    it('can add content', contentAdd);
    it('content exists', contentExists);
    it('can logout', logout);

    it('can restart app', async function () {
        execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS);
        await browser.sleep(20000);
    });

    it('clear cache', clearCache);
    it('can login as admin', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));
    it('content exists', contentExists);
    it('can logout', logout);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', async function () {
        execSync(`cloudron restore --app ${app.id}`, EXEC_ARGS);
        await browser.sleep(20000);
    });

    it('clear cache', clearCache);
    it('can login as admin', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));
    it('content exists', contentExists);
    it('can logout', logout);

    it('move to different location', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
        await browser.sleep(20000);
    });

    it('can get app information', getAppInfo);
    it('clear cache', clearCache);
    it('can login as admin', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));
    it('content exists', contentExists);
    it('can logout', logout);

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('install app for update', async function () {
        execSync(`cloudron install --appstore-id org.jellyfin.cloudronapp --location ${LOCATION}`, EXEC_ARGS);
        await browser.sleep(20000);
    });

    it('can get app information', getAppInfo);
    it('setup wizard', setupWizard);
    it('clear cache', clearCache);
    it('can login', login.bind(null, process.env.USERNAME, process.env.PASSWORD));
    it('can logout', logout);
    it('clear cache', clearCache);
    it('can login as admin', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));
    it('can add content', contentAdd);
    it('content exists', contentExists);
    it('can logout', logout);

    it('can update', function () { execSync(`cloudron update --app ${app.id}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);

    it('clear cache', clearCache);
    it('can login', login.bind(null, process.env.USERNAME, process.env.PASSWORD));
    it('can logout', logout);

    it('clear cache', clearCache);
    it('can login as admin', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));
    it('content exists', contentExists);

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});
