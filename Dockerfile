FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/pkg
WORKDIR /app/pkg

# https://repo.jellyfin.org/ (https://repo.jellyfin.org/?path=/server/ubuntu/latest-stable/amd64)
# renovate: datasource=github-releases depName=jellyfin/jellyfin versioning=semver extractVersion=^v(?<version>.+)$
ARG JELLYFIN_VERSION="10.10.6"
# https://repo.jellyfin.org/releases/plugin/ldap-authentication/
# renovate: datasource=github-releases depName=jellyfin/jellyfin-plugin-ldapauth versioning=regex:^(?<major>\d+)$ extractVersion=^v(?<version>.+)$
ARG JELLYFIN_LDAP_VERSION="19"

ENV DEBIAN_FRONTEND="noninteractive"
ENV NVIDIA_DRIVER_CAPABILITIES="compute,video,utility"

RUN curl -o /tmp/key.gpg.key https://repo.jellyfin.org/ubuntu/jellyfin_team.gpg.key && apt-key add /tmp/key.gpg.key && \
    echo 'deb [arch=amd64] https://repo.jellyfin.org/ubuntu jammy main' > /etc/apt/sources.list.d/jellyfin.list

# RUN apt-get update && apt search jellyfin && exit 1

RUN apt-get update && \
    apt-get install -y --no-install-recommends at vainfo libva2 i965-va-driver jellyfin-ffmpeg6 mesa-va-drivers && \
    apt-get install -y "jellyfin=${JELLYFIN_VERSION}*" && \
    mkdir -p /tmp/jellyfin_ldap /app/code/jellyfin_ldap && \
    wget "https://repo.jellyfin.org/releases/plugin/ldap-authentication/ldap-authentication_${JELLYFIN_LDAP_VERSION}.0.0.0.zip" -O /tmp/jellyfin_ldap/jellyfin_ldap.zip && \
    unzip /tmp/jellyfin_ldap/jellyfin_ldap.zip -d /app/code/jellyfin_ldap && \
    rm -rf /tmp/* /var/lib/apt/lists/* /var/tmp/*

# Workaround for https://github.com/jellyfin/jellyfin/issues/3638 / https://github.com/jellyfin/jellyfin/issues/3956
RUN ln -s /usr/share/jellyfin/web/ /usr/lib/jellyfin/bin/jellyfin-web

# Jellyfin needs to write plugin manifest stuff
RUN mv /app/code/jellyfin_ldap/meta.json /app/code/jellyfin_ldap/meta.json.orig && \
    ln -s /app/data/jellyfin/jellyfin_ldap.meta.json /app/code/jellyfin_ldap/meta.json

COPY LDAP-Auth.xml.template start.sh /app/pkg/

CMD ["/app/pkg/start.sh"]
