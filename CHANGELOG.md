[1.0.0]
* Initial version

[1.0.1]
* Fix SFTP permissions

[1.1.0]
* Update Jellyfin to 10.6.4

[1.1.1]
* Specify ffmpeg path for video playback

[1.1.2]
* Update to base image v3

[1.1.3]
* Update Jellyfin to 10.7.1
* SECURITY GHSL-2021-050 [@EraYaN] Fix issues 1 through 5 from GHSL-2021-050
* #5559 [@cvium] Clean the entity name for non-words before searching
* #5550 [@cvium] revert underscore as a multiversion separator
* #5532 [@cvium] do not resolve episode-like files if they are in extras folders
* #5518 [@crobibero] Add missing InstantMix endpoints
* #5515 [@cvium] fix refresh endpoint
* #5512 [@crobibero] Set openapi version to server version
* #5510 [@BaronGreenback] Fix: Streaming crashing due to no deviceProfileId match.
* #5504 [@crobibero] Add JsonStringConverter
* #5500 [@crobibero] Fix third party integration
* #5480 [@crobibero] Add SessionMessageType to generated openapi spec
* #5476 [@EraYaN] Remove BuildPackage dependency for PublishNuget in CI
* #5475 [@BaronGreenback] Null pointer fix in DLNA when its disabled.
* #5461 [@cvium] fix multiversion eligibility check for complex folder names
* #5457 [@cvium] Use distinct for AllArtists to avoid double refreshing
* #5447 [@joshuaboniface] Remove Microsoft repo from install step
* #5444 [@Ullmie02] Use FileShare.Read to fix HdHomeRun
* #5431 [@cvium] Use imdbid as fallback in movie provider
* #5428 [@cvium] Default to the searchinfo year, fallback to parsed year
* #5403 [@BaronGreenback] Various DLNA Device Profile fixes
* #5324 [@danieladov] Fix duplicated movies when group movies into collections is enabled
* jellyfin-web#2503 [@thornbill] Fix default values for invalid config.json files
* jellyfin-web#2502 [@brianjmurrell] Add BR: nodejs for Fedora 33 and up
* jellyfin-web#2498 [@thornbill] Add close button to comics player
* jellyfin-web#2496 [@thornbill] Allow logos without backdrops enabled

[1.1.4]
* Update Jellyfin to 10.7.2

[1.1.5]
* Update Jellyfin to 10.7.5
* Security advisory https://github.com/jellyfin/jellyfin/security/advisories/GHSA-rgjw-4fwc-9v96: Remove dangerous endpoints that allow unauthenticated enumeration and access to private HTTP resources. NOTE: This is an API-breaking change. The major security risk outweights the issue of changing the endpoints. These were used only when fetching remote images.

[1.1.6]
* Update Jellyfin to 10.7.6
* Fix DLNA/SSDP flooding bug

[1.1.7]
* Update Jellyfin to 10.7.7
* #6512 [@thornbill] Preferences fix
* #6274 [@thornbill] Restore max width and height params
* jellyfin/jellyfin-web#2759 [@thornbill] Fix serviceworker paths

[1.2.0]
* Make plugin listing and installation work

[1.2.1]
* Update base image to 3.2.0

[1.3.0]
* Update Jellyfin to 10.8.0
* [Full changelog](https://github.com/jellyfin/jellyfin/releases/tag/v10.8.0)

[1.3.1]
* Update Jellyfin to 10.8.1
* [Full changelog](https://github.com/jellyfin/jellyfin/releases/tag/v10.8.1)

[1.3.2]
* Update Jellyfin to 10.8.3
* [Full changelog](https://github.com/jellyfin/jellyfin/releases/tag/v10.8.3)
* Update LDAP plugin to 16.0.0.0

[1.3.3]
* Update Jellyfin to 10.8.4
* [Full changelog](https://github.com/jellyfin/jellyfin/releases/tag/v11.8.4)

[1.3.4]
* `chown` data directory only if needed

[1.3.5]
* Update Jellyfin to 10.8.5
* [Full changelog](https://github.com/jellyfin/jellyfin/releases/tag/v11.8.5)

[1.3.6]
* Update Jellyfin to 10.8.6
* [Full changelog](https://github.com/jellyfin/jellyfin/releases/tag/v10.8.6)

[1.3.7]
* Udate Jellyfin to 10.8.7
* [Full changelog](https://github.com/jellyfin/jellyfin/releases/tag/v10.8.7)

[1.3.8]
* Update Jellyfin to 10.8.8
* [Full changelog](https://github.com/jellyfin/jellyfin/releases/tag/v10.8.8)
* #8753 [@thornbill] Fix items access backport
* jellyfin/jellyfin-web#4172 [@thornbill] Fix item details banner image alignment
* jellyfin/jellyfin-web#4166 [@dmitrylyzo] Fix keyboard navigation for INPUT and TEXTAREA

[1.4.0]
* Update base image to 4.0.0

[1.5.0]
* VAAPI support

[1.5.1]
* Update Jellyfin to 10.8.9
* [Full changelog](https://github.com/jellyfin/jellyfin/releases/tag/v10.8.9)
* #9145 [@nyanmisaka] Fix PGS position issue in sw decoding #8602
* #9130 [@Shadowghost] Fix API access permissions for LiveTV and downloads
* #9112 [@nyanmisaka] Backport the ffmpeg link fix (#8901) to 10.8.z
* #9092 [@dmitrylyzo] Don't add additional entries if HEVC encoding is disabled
* #9060 [@thornbill] Disable splash screen image by default
* #9051 [@dmitrylyzo] Fix transcode reasons
* #9050 [@nyanmisaka] Update workaround for i915 hang in linux 5.18 to 6.1.3
* #9049 [@Shadowghost] Add more codecs requiring ffmpeg strict -2
* #9020 [@Bond-009] Backport 8726: Fix incorrect starting offset of buffer span in CheckTunerAvailability
* #9009 [@dmitrylyzo] Fix secondary audio
* jellyfin/jellyfin-web#4288 [@dmitrylyzo] Fix loading Spanish (Latin America) (es-419)
* jellyfin/jellyfin-web#4274 [@dmitrylyzo] Fix back action on Hisense TV
* jellyfin/jellyfin-web#4269 [@thornbill] Fix XSS vulnerability in plugin repo pages
* jellyfin/jellyfin-web#4267 [@dmitrylyzo] Fix AudioContext limit exceeded
* jellyfin/jellyfin-web#4263 [@dmitrylyzo] Fix change audio track
* jellyfin/jellyfin-web#4243 [@dmitrylyzo] Fix HTML escaping in MediaSession and on remote page
* jellyfin/jellyfin-web#4240 [@dmitrylyzo] Fix detection of SecondaryAudio support
* jellyfin/jellyfin-web#4238 [@thornbill] Fix XSS vulnerability in router
* jellyfin/jellyfin-web#4182 [@nyanmisaka] Fix the progressive mp4 transcoding profile

[1.5.2]
* Update Jellyfin to 10.8.10
* [Full changelog](https://github.com/jellyfin/jellyfin/releases/tag/v10.8.10)
* Update Jellyfin LDAP to 17.0.0.0
* CRITICAL SECURITY ADVISORY: GHSA-9p5f-5x8v-x65m and GHSA-89hp-h43h-r5pq can be combined to allow remote code execution for any authenticated Jellyfin user including non-admin users

[1.5.3]
* Update Jellyfin to 10.8.11
* [Full changelog](https://github.com/jellyfin/jellyfin/releases/tag/v10.8.11)
* Stable hotfix release for 10.8.z release branch.

[1.6.0]
* Update base image to 4.2.0

[1.6.1]
* Update Jellyfin to 10.8.12
* [Full changelog](https://github.com/jellyfin/jellyfin/releases/tag/v10.8.12)

[1.6.2]
* Update Jellyfin to 10.8.13
* [Full changelog](https://github.com/jellyfin/jellyfin/releases/tag/v10.8.13)

[1.7.0]
* Persist LDAP plugin configuration across restarts
* Enable access to all folders by default for users

[1.8.0]
* Update Jellyfin to 10.9.1
* Update LDAP plugin to 19.0.0.0
* [Full changelog](https://github.com/jellyfin/jellyfin/releases/tag/v10.9.1)

[1.8.1]
* Update Jellyfin to 10.9.2
* [Full changelog](https://github.com/jellyfin/jellyfin/releases/tag/v10.9.2)
* Fix FirstTimeSetupPolicy allowing guest access [PR #11651], by @thornbill
* Fix series status parsing [PR #11648], by @Shadowghost
* Fix season names [PR #11647], by @Shadowghost
* Secure local playlist path handling [PR #11680], by @Shadowghost
* Prevent double iterating over all seasons [PR #11700], by @Shadowghost
* Fix network binding [PR #11671], by @Shadowghost
* Workaround ffmpeg keyframe seeking for external subtitles [PR #11689], by @gnattu
* Use MediaType instead of ToString and add text/ as disallowed mimetypes [PR #11699], by @cvium
* Fix not binding to SQL parameters [PR #11698], by @Bond-009
* Fix local playlist scanning [PR #11673], by @Shadowghost
* Restore caching for UserManager [PR #11670], by @gnattu
* Properly dispose dbContext in MigrateUserDb [PR #11677], by @Bond-009
* Fix quality parameter for vaapi_mjpeg [PR #11675], by @gnattu
* Always fallback for failed HEAD request [PR #11668], by @gnattu
* Don't generate TrickPlay images for files that don't exist [PR #11653], by @Bond-009
* Allow empty user id when getting device list [PR #11633], by @crobibero
* Fix missing filename for timer [PR #11629], by @gnattu
* Handle exception for unexpected audio file YEAR tag values [PR #11621], by @nfmccrina
* Fix network config [PR #11587], by @gnattu

[1.8.2]
* Update Jellyfin to 10.9.3
* [Full changelog](https://github.com/jellyfin/jellyfin/releases/tag/v10.9.3)
* Extract media attachment one by one if the filename appears to be a path [PR #11812], by @gnattu
* Filter invalid IPs on external interface matching [PR #11766], by @gnattu
* Use SharedStream for LiveTV more restrictively [PR #11805], by @gnattu
* Fix the IOSurf error in QSV transcoding [PR #11830], by @nyanmisaka
* Improve reliability of HasChanged check [PR #11792], by @Shadowghost
* Trickplay: kill ffmpeg when task is cancelled [PR #11790], by @NotSaifA
* Force more compatible transcoding profile for LiveTV [PR #11801], by @gnattu
* Exclude virtual items from DateLastMediaAdded calculation [PR #11804], by @Shadowghost

[1.8.3]
* Update Jellyfin to 10.9.4
* [Full changelog](https://github.com/jellyfin/jellyfin/releases/tag/v10.9.4)
* Security: Fix FirstTimeSetupHandler allowing public access [PR #11873], by @thornbill
* Fix replace logic [PR #11743], by @Shadowghost
* Mark Audio as RequiresDeserialization and backfill data [PR #11762], by @Shadowghost
* Move NFO series season name parsing to own local provider [PR #11719], by @Shadowghost
* Audio normalization: parse ffmpeg output line by line [PR #11910], by @Bond-009
* Relax remuxing requirement for LiveTV [PR #11851], by @gnattu
* Fix multi-part album folder being detected as artist folder [PR #11886], by @gnattu
* Use music metadata from ffprobe when TagLib fails [PR #11859], by @gnattu
* Return missing episodes for series when no user defined [PR #11806], by @Shadowghost

[1.8.4]
* Update Jellyfin to 10.9.5
* [Full changelog](https://github.com/jellyfin/jellyfin/releases/tag/v10.9.5)
* Fallback to local dir when saving to media dir fails [PR #11978], by @Shadowghost
* Fix Library renaming [PR #11963], by @gnattu
* Fix identify over NFO and replace all when NFO saving enabled [PR #11921], by @Shadowghost
* Create readonly DB connections when possible [PR #11969], by @Bond-009
* Fix local image saving [PR #11934], by @Shadowghost

[1.8.5]
* Update Jellyfin to 10.9.6
* [Full changelog](https://github.com/jellyfin/jellyfin/releases/tag/v10.9.6)
* Fix fallback artist when taglib fails [PR #11989], by @gnattu
* Do not stop validation if folder was removed [PR #11959], by @Shadowghost
* Use only 1 write connection/DB [PR #11986], by @Bond-009
* Set ProductionLocations instead of Tags [PR #11984], by @Shadowghost

[1.8.6]
* Update Jellyfin to 10.9.7
* [Full changelog](https://github.com/jellyfin/jellyfin/releases/tag/v10.9.7)
* Fix HDR detection for 4K Blu-Ray BDMVs [PR #12166], by @Bond-009
* Log album name and id in normalization task [PR #11911], by @Bond-009
* Try to add extracted lyrics during scanning [PR #12126], by @gnattu
* Fix season backdrops [PR #12055], by @Shadowghost
* Rewrite PlaylistItemsProvider as ILocalMetadataProvider [PR #12053], by @Shadowghost
* Fix empty image folder removal for legacy locations [PR #12025], by @Shadowghost

[1.8.7]
* Update Jellyfin to 10.9.8
* [Full changelog](https://github.com/jellyfin/jellyfin/releases/tag/v10.9.8)
* Properly escape paths in concat file for BDMV [PR #12296], by @Bond-009
* Fix localization of audio title [PR #12278], by @Bond-009
* Fix season handling ("Season Unknown" / unneccesary empty seasons) [PR #12240], by @nielsvanvelzen

[1.8.8]
* Update Jellyfin to 10.9.9
* [Full changelog](https://github.com/jellyfin/jellyfin/releases/tag/v10.9.9)
* Fix creating virtual seasons (again) [PR #12356], by @nielsvanvelzen
* Update Serilog deps [PR #12368], by @Bond-009
* Implement Device Cache to replace EFCoreSecondLevelCacheInterceptor [PR #11901], by @gnattu

[1.8.9]
* Update Jellyfin to 10.9.10
* [Full changelog](https://github.com/jellyfin/jellyfin/releases/tag/v10.9.10)
* Include AVIF extension for support images [PR #12415], by @ikelos
* Apply all codec conditions [PR #12499], by @dmitrylyzo
* Revert "NextUp query respects Limit" [PR #12414], by @fredrik-eriksson
* Set Content-Disposition header to attachment for image endpoints [PR #12490], by @nielsvanvelzen
* Fix bitstream filter not applied to videos in TS container [PR #12493], by @nyanmisaka
* Fix the record series button missing on many programs (port of #12398) [PR #12481], by @Bond-009
* Don't force non-virtual when all episodes in season are isMissing=true [PR #12425], by @scampower3
* Check attachment path for null before use [PR #12443], by @gnattu

[1.8.10]
* Update Jellyfin to 10.9.11
* [Full changelog](https://github.com/jellyfin/jellyfin/releases/tag/v10.9.11)
* Fix subtitle and attachment extraction when input path contains quotes [PR #12575], by @dmitrylyzo
* Use filtered codecs to build appliedConditions [PR #12562], by @nyanmisaka
* Fix alt version name generation [PR #12558], by @Bond-009
* Create and use FormattingStreamWriter [PR #12550], by @Bond-009
* Fix CodecProfiles and video encoder profiles [PR #12521], by @nyanmisaka
* Don't apply chapter image settings to music [PR #12531], by @gnattu

[1.9.0]
* Update Jellyfin to 10.10.0
* [Full changelog](https://github.com/jellyfin/jellyfin/releases/tag/v10.10.0)
* Fix OpenAPI workflow [PR #11733], by @nielsvanvelzen
* Feature/media segments plugin api [PR #12359], by @JPVenson
* Add media segments API [PR #12345], by @JPVenson
* Enable Dolby AC-4 decoder [PR #11486], by @gnattu
* Use real temp dir instead of cache dir for temp files [PR #12226], by @Bond-009
* Add software tonemap filter support [PR #12270], by @gnattu


[1.9.1]
* Update Jellyfin to 10.10.1
* Set AudioCodec when building stream \[MR [#&#8203;12931](https://github.com/jellyfin/jellyfin/issues/12931)], by [@&#8203;gnattu](https://github.com/gnattu)
* Remove DynamicImageResponse local image after saved to metadata folder \[MR [#&#8203;12940](https://github.com/jellyfin/jellyfin/issues/12940)], by [@&#8203;gnattu](https://github.com/gnattu)
* Fixed possible NullReferenceException in SessionManager \[MR [#&#8203;12915](https://github.com/jellyfin/jellyfin/issues/12915)], by [@&#8203;JPVenson](https://github.com/JPVenson)
* Don't try to prune images for virtual episodes. \[MR [#&#8203;12909](https://github.com/jellyfin/jellyfin/issues/12909)], by [@&#8203;revam](https://github.com/revam)
* Fix TMDB import failing when no IMDB ID is set for a movie \[MR [#&#8203;12891](https://github.com/jellyfin/jellyfin/issues/12891)], by [@&#8203;benedikt257](https://github.com/benedikt257)
[1.9.2]
* Update jellyfin to 10.10.2
* [Full Changelog](https://github.com/jellyfin/jellyfin/releases/tag/v10.10.0)
* Added query filter to disregard disabled Providers \[MR [#&#8203;12916](https://github.com/jellyfin/jellyfin/issues/12916)], by [@&#8203;JPVenson](https://github.com/JPVenson)
* Respect cancellation token/HTTP request aborts correctly in `SymlinkFollowingPhysicalFileResultExecutor` \[MR [#&#8203;13033](https://github.com/jellyfin/jellyfin/issues/13033)], by [@&#8203;goknsh](https://github.com/goknsh)
* Update dependencies \[MR [#&#8203;13038](https://github.com/jellyfin/jellyfin/issues/13038)], by [@&#8203;Bond-009](https://github.com/Bond-009)
* Fix playlists \[MR [#&#8203;12934](https://github.com/jellyfin/jellyfin/issues/12934)], by [@&#8203;Shadowghost](https://github.com/Shadowghost)
* Fix missing procamp vaapi filter \[MR [#&#8203;13026](https://github.com/jellyfin/jellyfin/issues/13026)], by [@&#8203;nyanmisaka](https://github.com/nyanmisaka)
* Only set first MusicBrainz ID for audio tags \[MR [#&#8203;13003](https://github.com/jellyfin/jellyfin/issues/13003)], by [@&#8203;gnattu](https://github.com/gnattu)

[1.9.3]
* Update jellyfin to 10.10.3
* [Full Changelog](https://github.com/jellyfin/jellyfin/releases/tag/v10.10.0)
* Exclude file system based library playlists from migration \[MR [#&#8203;13059](https://github.com/jellyfin/jellyfin/issues/13059)], by [@&#8203;Shadowghost](https://github.com/Shadowghost)
* Downgrade minimum sdk version \[MR [#&#8203;13063](https://github.com/jellyfin/jellyfin/issues/13063)], by [@&#8203;crobibero](https://github.com/crobibero)

[1.9.4]
* Update jellyfin to 10.10.4
* [Full Changelog](https://github.com/jellyfin/jellyfin/releases/tag/v10.10.0)
* Never treat matroska as webm for audio playback \[MR [#&#8203;13345](https://github.com/jellyfin/jellyfin/issues/13345)], by [@&#8203;gnattu](https://github.com/gnattu)
* Don't generate trickplay for backdrops \[MR [#&#8203;13183](https://github.com/jellyfin/jellyfin/issues/13183)], by [@&#8203;gnattu](https://github.com/gnattu)
* Use nv15 as intermediate format for 2-pass rkrga scaling \[MR [#&#8203;13313](https://github.com/jellyfin/jellyfin/issues/13313)], by [@&#8203;gnattu](https://github.com/gnattu)
* Fix DTS in HLS \[MR [#&#8203;13288](https://github.com/jellyfin/jellyfin/issues/13288)], by [@&#8203;Shadowghost](https://github.com/Shadowghost)
* Transcode to audio codec satisfied other conditions when copy check failed. \[MR [#&#8203;13209](https://github.com/jellyfin/jellyfin/issues/13209)], by [@&#8203;gnattu](https://github.com/gnattu)
* Fix missing episode removal \[MR [#&#8203;13218](https://github.com/jellyfin/jellyfin/issues/13218)], by [@&#8203;Shadowghost](https://github.com/Shadowghost)
* Fix NFO ID parsing \[MR [#&#8203;13167](https://github.com/jellyfin/jellyfin/issues/13167)], by [@&#8203;Shadowghost](https://github.com/Shadowghost)
* Always do tone-mapping for HDR transcoding when software pipeline is used \[MR [#&#8203;13151](https://github.com/jellyfin/jellyfin/issues/13151)], by [@&#8203;nyanmisaka](https://github.com/nyanmisaka)

[1.9.5]
* Update jellyfin to 10.10.5
* [Full Changelog](https://github.com/jellyfin/jellyfin/releases/tag/v10.10.5)
* Add check to prevent downgrade from future EFCore refactor \[MR [#&#8203;13103](https://github.com/jellyfin/jellyfin/issues/13103)], by [@&#8203;JPVenson](https://github.com/JPVenson)
* Open files with FileShare.Read for BlurHash calculations \[MR [#&#8203;13425](https://github.com/jellyfin/jellyfin/issues/13425)], by [@&#8203;Bond-009](https://github.com/Bond-009)
* Don't select audio stream and codec explicitly for copy when bitrate exceeds limit \[MR [#&#8203;13423](https://github.com/jellyfin/jellyfin/issues/13423)], by [@&#8203;gnattu](https://github.com/gnattu)
* Fix parallel use of not thread-safe SubtitleFormat instance \[MR [#&#8203;13384](https://github.com/jellyfin/jellyfin/issues/13384)], by [@&#8203;alltilla](https://github.com/alltilla)
* Use WriteThrough for ImageSaver \[MR [#&#8203;13411](https://github.com/jellyfin/jellyfin/issues/13411)], by [@&#8203;gnattu](https://github.com/gnattu)
* Catch IOExceptions for GetFileSystemMetadata \[MR [#&#8203;13390](https://github.com/jellyfin/jellyfin/issues/13390)], by [@&#8203;gnattu](https://github.com/gnattu)
* Fix rating levels \[MR [#&#8203;13388](https://github.com/jellyfin/jellyfin/issues/13388)], by [@&#8203;Shadowghost](https://github.com/Shadowghost)
* Fix: handling of <set> elements in NfoParser \[MR [#&#8203;13092](https://github.com/jellyfin/jellyfin/issues/13092)], by [@&#8203;TheMelmacian](https://github.com/TheMelmacian)
* Fix interface selection \[MR [#&#8203;13382](https://github.com/jellyfin/jellyfin/issues/13382)], by [@&#8203;Shadowghost](https://github.com/Shadowghost)

[1.9.6]
* Update jellyfin to 10.10.6
* [Full Changelog](https://github.com/jellyfin/jellyfin/releases/tag/v10.10.6)
* Crashes on Apple silicon \[Issue [#&#8203;13379](https://github.com/jellyfin/jellyfin/issues/13379)] should be fixed by the bump to .NET framework version 8.0.13 in this release
* Update dependency z440.atl.core to 6.16.0 \[MR [#&#8203;13526](https://github.com/jellyfin/jellyfin/issues/13526)], by [@&#8203;crobibero](https://github.com/crobibero)
* Fix subnet contains check \[MR [#&#8203;13489](https://github.com/jellyfin/jellyfin/issues/13489)], by [@&#8203;gnattu](https://github.com/gnattu)
* Fix image encoding concurrency limit \[MR [#&#8203;13532](https://github.com/jellyfin/jellyfin/issues/13532)], by [@&#8203;gnattu](https://github.com/gnattu)
* Fix SchedulesDirect image prefetching \[MR [#&#8203;13469](https://github.com/jellyfin/jellyfin/issues/13469)], by [@&#8203;Shadowghost](https://github.com/Shadowghost)
* Skip allowed tags check for parents of an item \[MR [#&#8203;12721](https://github.com/jellyfin/jellyfin/issues/12721)], by [@&#8203;elfalem](https://github.com/elfalem)
* Fix LiveTV Guide Backdrop image not updating \[MR [#&#8203;13504](https://github.com/jellyfin/jellyfin/issues/13504)], by [@&#8203;IDisposable](https://github.com/IDisposable)
* Correctly handle audio number tag fallbacks \[MR [#&#8203;13490](https://github.com/jellyfin/jellyfin/issues/13490)], by [@&#8203;gnattu](https://github.com/gnattu)
* Allow api key to subscribe to admin websockets \[MR [#&#8203;13499](https://github.com/jellyfin/jellyfin/issues/13499)], by [@&#8203;crobibero](https://github.com/crobibero)
* Fix rating mistake in us.csv \[MR [#&#8203;13487](https://github.com/jellyfin/jellyfin/issues/13487)], by [@&#8203;Bond-009](https://github.com/Bond-009)
* Fall back to calculating mime type from path when needed \[MR [#&#8203;13439](https://github.com/jellyfin/jellyfin/issues/13439)], by [@&#8203;Bond-009](https://github.com/Bond-009)
* Fix interface ordering again \[MR [#&#8203;13448](https://github.com/jellyfin/jellyfin/issues/13448)], by [@&#8203;Shadowghost](https://github.com/Shadowghost)
* Fixed Websocket not locking state correctly \[MR [#&#8203;13459](https://github.com/jellyfin/jellyfin/issues/13459)], by [@&#8203;JPVenson](https://github.com/JPVenson)

