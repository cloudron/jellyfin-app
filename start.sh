#!/bin/bash

set -eu

export NODE_ENV=production

mkdir -p /app/data/jellyfin/data/data \
    /app/data/jellyfin/data/plugins/ \
    /app/data/jellyfin/config/users/admin \
    /app/data/jellyfin/log \
    /app/data/jellyfin/data/plugins/configurations/ \
    /app/data/files/TV\ Shows/ \
    /app/data/files/Movies/ \
    /run/jellyfin/cache

cp /app/code/jellyfin_ldap/meta.json.orig /app/data/jellyfin/jellyfin_ldap.meta.json

# Copy over latest LDAP plugin. Do not symlink because jellyfin writes to this directory
rm -rf "/app/data/jellyfin/data/plugins/LDAP Authentication"
mkdir "/app/data/jellyfin/data/plugins/LDAP Authentication/"
cp -r /app/code/jellyfin_ldap/* "/app/data/jellyfin/data/plugins/LDAP Authentication/"

if [[ ! -f /app/data/jellyfin/data/plugins/configurations/LDAP-Auth.xml ]]; then
    cp /app/pkg/LDAP-Auth.xml.template /app/data/jellyfin/data/plugins/configurations/LDAP-Auth.xml
fi

echo "==> Updating LDAP configuration"
xmlstarlet ed --inplace \
    --update '//PluginConfiguration/LdapServer' -v "${CLOUDRON_LDAP_SERVER}" \
    --update '//PluginConfiguration/LdapBaseDn' -v "${CLOUDRON_LDAP_USERS_BASE_DN}" \
    --update '//PluginConfiguration/LdapPort' -v "${CLOUDRON_LDAP_PORT}" \
    --update '//PluginConfiguration/LdapBindUser' -v "${CLOUDRON_LDAP_BIND_DN}" \
    --update '//PluginConfiguration/LdapBindPassword' -v "${CLOUDRON_LDAP_BIND_PASSWORD}" \
    /app/data/jellyfin/data/plugins/configurations/LDAP-Auth.xml

echo "==> Changing ownership"
chown -R cloudron:cloudron /run/jellyfin
[[ $(stat --format '%U' /app/data/data) != "cloudron" ]] && chown -R cloudron:cloudron /app/data

echo "==> Starting Jellyfin"
exec /usr/bin/jellyfin --datadir /app/data/jellyfin/data --configdir /app/data/jellyfin/config --logdir /app/data/jellyfin/log --cachedir /run/jellyfin/cache --package-name cloudron-jellyfin --ffmpeg /usr/share/jellyfin-ffmpeg/ffmpeg
