# Jellyfin Cloudron App

This repository contains the Cloudron app package source for [Jellyfin](https://jellyfin.org/).

## Installation

[![Install](https://cloudron.io/img/button.svg)](https://cloudron.io/button.html?app=org.jellyfin.cloudronapp)

or using the [Cloudron command line tooling](https://cloudron.io/references/cli.html)

```
cloudron install --appstore-id org.jellyfin.cloudronapp
```

## Building

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
cd jellyfin-app

cloudron build
cloudron install
```

## Testing

The e2e tests are located in the `test/` folder and require [nodejs](http://nodejs.org/). They are creating a fresh build, install the app on your Cloudron, backup and restore.

```
cd jellyfin-app/test

npm install
EMAIL=<cloudron account email> USERNAME=<cloudron username> PASSWORD=<cloudron password> mocha --bail test.js
```
